//
//  NetworkProtocols.swift
//  NewsApp
//
//  Created by Raj on 17/04/21.
//

import Foundation

protocol Networking {
    func request(urlString: String, completion: @escaping (Data?, ErrorType?) -> Void)
}

protocol DataFetchers {
    func fetchGenericJSONData<T: Decodable>(urlString: String, response: @escaping (T?) -> Void)
}

enum ErrorType: Error {
    case optionalUnwrapError
    case noData
}
