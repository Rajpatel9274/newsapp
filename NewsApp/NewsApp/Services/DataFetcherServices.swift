//
//  DataFetcherServices.swift
//  NewsApp
//
//  Created by Raj on 17/04/21.
//

import Foundation

class DataFetcherServices {
    
    var dataFetcher: DataFetchers
    
    private let sources = "https://newsapi.org/v2/sources?apiKey="
    private let everything = "https://newsapi.org/v2/everything?q=bitcoin&apiKey="
    private let apiKey = "9ca2358a8aca4b128f3048ddc6cdb582"
    private let topHeadLines = "https://newsapi.org/v2/top-headlines?country=us&apiKey="

    init(dataFetcher: DataFetchers = NetworkDataFetchers()) {
        self.dataFetcher = dataFetcher
    }
    
    func fetchTopHeadLines(completion: @escaping (NewsModel?) -> Void) {
        let topHeadLinesURL = topHeadLines + apiKey
        dataFetcher.fetchGenericJSONData(urlString: topHeadLinesURL, response: completion)
    }
    
    func fetchSources(completion: @escaping (NewsModel?) -> Void) {
        let sourcesURL = everything + apiKey
        dataFetcher.fetchGenericJSONData(urlString: sourcesURL, response: completion)
    }
    
    func fetchEverything(completion: @escaping (NewsModel?) -> Void) {
        let everythingURL = everything + apiKey
        dataFetcher.fetchGenericJSONData(urlString: everythingURL, response: completion)
    }
}
