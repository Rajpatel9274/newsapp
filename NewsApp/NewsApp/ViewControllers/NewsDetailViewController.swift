//
//  NewsDetailViewController.swift
//  NewsApp
//
//  Created by Raj on 17/04/21.
//

import UIKit
import WebKit

class NewsDetailViewController: UIViewController  {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let feedCellId = "FeedDeatilsTableViewCell"
    private var dataFetcherService = DataFetcherServices()
    var myFeed: NewsModel.Article?
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib.init(nibName:feedCellId, bundle: nil), forCellReuseIdentifier: feedCellId)
        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = 200
        tableView.estimatedRowHeight  = UITableView.automaticDimension
     }
    
 
    private func setImage(from url: URL ,feedImage: UIImageView) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                feedImage.image = UIImage(data: data)
            }
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}

extension NewsDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: feedCellId, for: indexPath) as! FeedDeatilsTableViewCell
        
        cell.titleLabel.text = myFeed?.title
             cell.descriptionLabel.text = myFeed?.description
            cell.activityView.isHidden = false
            cell.activityView.startAnimating()
            if myFeed?.urlToImage != nil {
                setImage(from: (myFeed?.urlToImage!)!, feedImage: cell.imageFeed!)
                self.registerForTapGesture(imageview: cell.imageFeed!)
                cell.activityView.stopAnimating()
            }
            cell.activityView.isHidden = true
 
        return cell
    }
}

 
