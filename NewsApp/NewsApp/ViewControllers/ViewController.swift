//
//  ViewController.swift
//  NewsApp
//
//  Created by Raj on 17/04/21.
//

import UIKit
import Agrume

class ViewController: UIViewController , UISearchControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let feedCellId = "FeedTblViewCell"
    private var dataFetcherService = DataFetcherServices()
    private var myFeed: [NewsModel.Article?] = []
    private let searchController = UISearchController(searchResultsController: nil)
    private var filteredData: [NewsModel.Article?] = []
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return !searchBarIsEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        tableView.register(UINib.init(nibName:feedCellId, bundle: nil), forCellReuseIdentifier: feedCellId)
        tableView.separatorColor = UIColor.clear
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        
        fetchTopHeadlines()
    }
    
    private func fetchTopHeadlines() {
        dataFetcherService.fetchTopHeadLines { [weak self] (myFeed) in
            guard let `self` = self, let myFeed = myFeed?.articles else { return }
            self.myFeed = myFeed
            self.filteredData = myFeed
            self.tableView.reloadData()
        }
    }
    private func setImage(from url: URL ,feedImage: UIImageView) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                feedImage.image = UIImage(data: data)
            }
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredData.count
        }
        return myFeed.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: feedCellId, for: indexPath) as! FeedTblViewCell
        let articles: NewsModel.Article?
        if !isFiltering {
            articles = self.myFeed[indexPath.row]
        } else {
            articles = self.filteredData[indexPath.row]
        }
        if articles != nil {
            cell.titleLabel.text = articles?.title
            cell.sourceLabel.text = articles!.source.name
            cell.authorLabel.text = articles?.author
            cell.descriptionLabel.text = articles?.description
            cell.activityView.isHidden = false
            cell.activityView.startAnimating()
            if articles?.urlToImage != nil {
                setImage(from: (articles?.urlToImage!)!, feedImage: cell.imageFeed!)
                self.registerForTapGesture(imageview: cell.imageFeed!)
                cell.activityView.stopAnimating()
            }
            cell.activityView.isHidden = true
        }
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let articles: NewsModel.Article?
        if !isFiltering {
            articles = self.myFeed[indexPath.row]
        } else {
            articles = self.filteredData[indexPath.row]
        }
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewsDetailViewController") as! NewsDetailViewController
        vc.myFeed = articles
        self.navigationController?.pushViewController (vc, animated: true)
    }
}

extension ViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    private func filterContentForSearchText(_ searchText: String ) {
        filteredData = myFeed.filter { (article: NewsModel.Article?) -> Bool in
            return (article?.title!.lowercased().contains(searchText.lowercased()))!
        }
        tableView.reloadData()
    }
}

extension UIViewController {

    //------------------------------
    func registerForTapGesture(imageview : UIImageView)
    {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector((self.imageTapped(tapGestureRecognizer:))))
        imageview.isUserInteractionEnabled = true
        imageview.addGestureRecognizer(tapGestureRecognizer)
    }

    // MARK: - Action
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as? UIImageView
        if tappedImage?.image != nil{
            let agrume = Agrume(image: (tappedImage?.image)!, background: Background.colored(.black))
            agrume.hideStatusBar = true
            agrume.show(from: self)
        }

    }
}
