//
//  FeedDeatilsTableViewCell.swift
//  NewsApp
//
//  Created by Raj on 17/04/21.
//

import UIKit

class FeedDeatilsTableViewCell: UITableViewCell {
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!
     @IBOutlet weak var imageFeed: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
     override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
