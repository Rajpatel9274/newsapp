//
//  NewsModel.swift
//  NewsApp
//
//  Created by Raj on 17/04/21.
//

import UIKit
import Foundation

struct NewsModel: Decodable {
    let status: String?
    let totalResults: Int?
    let articles: [Article]
    
    struct Article: Decodable {
        let source: Source
        let author: String?
        let title: String?
        let description: String?
        let url: URL?
        let urlToImage: URL?
        let publishedAt: String

        struct Source: Decodable {
            let id: String?
            let name: String?
        }
    }
}
